﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lab3
{
    public class Book
    {
        public string Name { get; set; }
        public string[] Authors { get; set; }
        public int GenreID { get; set; }
        public bool Available { get; set; }
        public int BookID { get; set; }
        public double Price { get; set; }
        public Book(string name, string[] authors, int genreID, bool available, int bookID, double price) 
        {
            Name = name;
            Authors = authors;
            GenreID = genreID;
            Available = available;
            BookID = bookID;
            Price = price;
        }
        public Book(string name, string[] authors, int genreID)
        {
            Name = name;
            Authors = authors;
            GenreID = genreID;
        }
        public override string ToString()
        {
            return String.Format("{0, -40}{1, -40}{2, -40}", this.Name, this.Authors, this.GenreID);
        }
        public override bool Equals(object obj)
        {
            Book other = obj as Book;
            return Name == other.Name && Authors[0] == other.Authors[0] && GenreID == other.GenreID;
        }
    }
    public class Genre
    {
        public string Name { get; set; }
        public int ID { get; set; }
    }
    public class ModelTest
    {
        public List<Genre> Genres
        {
            get
            {
                return new List<Genre>()
                {
                    new Genre{ Name = "Fiction", ID = 001 },
                    new Genre{ Name = "Graphic Novels", ID = 002 },
                    new Genre{ Name = "Historical Fiction", ID = 003 },
                    new Genre{ Name = "Nonfiction", ID = 004 }
                };
            }
        }
        public List<Book> Books
        {
            get
            {
                //return new List<Book>()
                //{
                //    new Book{ Name = "1984", Author = "George Orwell", GenreID = 001, Available = true, BookID = 1 },
                //    new Book{ Name = "To Kill a Mockingbird", Author = "Harper Lee", GenreID = 001, Available = false, BookID = 3 },
                //    new Book{ Name = "Animal Farm", Author = "George Orwell", GenreID = 001, Available = true, BookID = 2 },
                //    new Book{ Name = "Death Note", Author = "Tsugumi Ohba", GenreID = 002, Available = false, BookID = 5 },
                //    new Book{ Name = "Fullmetal Alchemist", Author = "Hiromu Arakawa", GenreID = 002, Available = true, BookID = 4 },
                //    new Book{ Name = "Death Note", Author = "Tsugumi Ohba", GenreID = 002, Available = false, BookID = 8 },
                //    new Book{ Name = "All the Light We Cannot See", Author = "Anthony Doerr", GenreID = 003, Available = true, BookID = 16 },
                //    new Book{ Name = "The Nightingale", Author = "Kristin Hannah", GenreID = 003, Available = false, BookID = 6 },
                //    new Book{ Name = "The Great Alone", Author = "Kristin Hannah", GenreID = 003, Available = true, BookID = 9 },
                //    new Book{ Name = "The Four Winds", Author = "Kristin Hannah", GenreID = 003, Available = false, BookID = 10 },
                //    new Book{ Name = "The Diary of a Young Girl", Author = "Anne Frank", GenreID = 004, Available = true, BookID = 11 },
                //    new Book{ Name = "Sapiens: A Brief History of Humankind", Author = "Yuval Noah Harari", GenreID = 004, Available = false, BookID = 12 },
                //    new Book{ Name = "Homo Deus: A History of Tomorrow", Author = "Yuval Noah Harari", GenreID = 004, Available = true, BookID = 13 },
                //    new Book{ Name = "A Brief History of Time", Author = "Stephen Hawking", GenreID = 004, Available = false, BookID = 14 },
                //    new Book{ Name = "Black Holes and Baby Universes", Author = "Stephen Hawking", GenreID = 004, Available = true, BookID = 15 },
                //    new Book{ Name = "Brief Answers to the Big Questions", Author = "Stephen Hawking", GenreID = 004, Available = false, BookID = 7 }

                //};
                return new List<Book>()
                {
                    new Book("1984", new string[] {"George Orwell",  "Harper Lee"}, 001, true, 1, 100.34),
                    new Book("To Kill a Mockingbird", new string[]{"Harper Lee" }, 001, false, 3, 120.90),
                    new Book("Animal Farm", new string[]{"George Orwell" },  001,  true,  2, 78.6),
                    new Book("Death Note", new string[]{"Tsugumi Ohba" }, 002, false, 5, 45.59),
                    new Book("Fullmetal Alchemist", new string[]{"Hiromu Arakawa", "Okasa Ory" }, 002, true, 4, 190.7),
                    new Book("Death Note", new string[]{"Tsugumi Ohba" }, 002, false, 8, 78.2),
                    new Book("All the Light We Cannot See", new string[]{"Anthony Doerr" }, 003, true, 16, 156),
                    new Book("The Nightingale", new string[]{"Kristin Hannah" }, 003, false, 6, 145.3),
                    new Book("The Great Alone", new string[]{"Kristin Hannah", "Keny John" }, 003, true, 9, 167.89),
                    new Book("The Four Winds", new string[]{"Kristin Hannah" }, 003, false, 10, 134.0),
                    new Book("The Diary of a Young Girl", new string[]{"Anne Frank" }, 004, true, 11, 190.8),
                    new Book("Sapiens: A Brief History of Humankind", new string[]{"Yuval Noah Harari" }, 004, false, 12, 100),
                    new Book("Homo Deus: A History of Tomorrow", new string[]{"Yuval Noah Harari" }, 004, true, 13, 99.9),
                    new Book("A Brief History of Time", new string[]{"Stephen Hawking" }, 004, false, 14, 84.7),
                    new Book("Black Holes and Baby Universes", new string[]{"Stephen Hawking" }, 004, true, 15, 56.56),
                    new Book("Brief Answers to the Big Questions", new string[]{"Stephen Hawking", "Kop Dulee" }, 004, false, 7, 115.8)
                };
            }
        }
        public Dictionary<int, List<Book>> dictionary
        {
            get
            {
                return Books.GroupBy(b => b.GenreID, b => b).ToDictionary(g => g.Key, g => g.ToList());
            }
        }
        public SortedList<int, Book> sortedList
        {
            get
            {
                SortedList<int, Book> list = new();
                Books.ForEach(item => list.Add(item.BookID, item));
                return list;
            }
        }
        public Queue<Book> queue
        {
            get
            {
                return new Queue<Book>(Books);
            }
        }
        public IOrderedEnumerable<IGrouping<string, Book>> grouping
        {
            get
            {
                return Books.GroupBy(b => b.Authors[0]).OrderByDescending(a => a.Count());
            }
        }
    
    }
    public static class ModelTestExtension
    {
        public static int BooksCount(this ModelTest model)
        {
            return model.Books.Count;
        }
        public static List<Book> SortByName(this List<Book> list)
        {
            return list.OrderBy(b => b.Name).ToList();
        }
        public static void ConsoleWrite(this List<Book> list)
        {
            list.ForEach(b => Console.WriteLine(b));
        }
        
    }
    public class CompareByName : IComparer<Book>
    {
        public int Compare(Book x, Book y)
        {
            if (x.Name.Length < y.Name.Length) return -1;
            else if (x.Name.Length > y.Name.Length) return 1;
            return 0;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ModelTest model = new ModelTest();
            var actualData = model.Books.GroupBy(b => b.Authors[0]).OrderByDescending(a => a.Count());
            model.Books.ForEach(b => Console.WriteLine(b));
        }
    }
}

