using System;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using lab3;

namespace TestLab3
{
    public class UnitTest1
    {
        [Fact]
        public void TestSelectList()
        {
            ModelTest list = new ModelTest();
            List<string[]> expectedData = new();
            foreach(Book b in list.Books)
            {
                expectedData.Add(b.Authors);
            }

            var actualData = list.Books.Select(b => b.Authors);

            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestWhereList()
        {
            ModelTest list = new ModelTest();
            List<Book> expectedData = new List<Book>();
            foreach (Book b in list.Books)
            {
                if (b.GenreID == 4) expectedData.Add(b);
            }

            var actualData = list.Books.Where(b => b.GenreID == 004);

            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestExtensionSort()
        {
            ModelTest model = new ModelTest();
            List<Book> expectedData = new();
            expectedData.Add(new Book("A Brief History of Time", new string[] { "Stephen Hawking" }, 004, false, 14, 84.7));
            expectedData.Add(new Book("Black Holes and Baby Universes", new string[] { "Stephen Hawking" }, 004, true, 15, 56.56));
            expectedData.Add(new Book("Brief Answers to the Big Questions", new string[] { "Stephen Hawking", "Kop Dulee" }, 004, false, 7, 115.8));
            expectedData.Add(new Book("Homo Deus: A History of Tomorrow", new string[] { "Yuval Noah Harari" }, 004, true, 13, 99.9));
            expectedData.Add(new Book("Sapiens: A Brief History of Humankind", new string[] { "Yuval Noah Harari" }, 004, false, 12, 100));
            expectedData.Add(new Book("The Diary of a Young Girl", new string[] { "Anne Frank" }, 004, true, 11, 190.8));

            var actualData = model.Books.Where(b => b.GenreID == model.Genres.Where(g => g.Name == "Nonfiction").Select(g => g.ID).FirstOrDefault()).ToList().SortByName();

            Assert.Equal(expectedData, actualData);    
        }
        [Fact]
        public void TestAnonymousTypes()
        {
            var model = new ModelTest();
            var expectedData = new Book("To Kill a Mockingbird", new string[] { "Harper Lee" }, 001, false, 3, 120.90);

            var actualData = model.Books.OrderBy(b => b.GenreID).ThenByDescending(b => b.Authors[0]).ThenBy(b => b.Name).First();

            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestIComparer()
        {
            ModelTest model = new();
            var expectedData = model.Books.OrderBy(b => b.Name.Length);

            var actualData = model.Books;
            actualData.Sort(new CompareByName());

            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestConvertListToArray()
        {
            ModelTest model = new();
            var expectedData = model.Books;

            var actualData = model.Books.ToArray();

            Assert.Equal(expectedData, actualData);
            Assert.IsType<Book[]>(actualData);
        }
        [Fact]
        public void TestSortByName()
        {
            ModelTest model = new();
            var expectedData = model.Books.SortByName();

            var actualData = model.Books.OrderBy(b => b.Name);

            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestAllTypesInModel()
        {
            ModelTest model = new();
            List<Book> expectedBoolData = new();
            model.Books.ForEach(b => { if (b.Available == true) expectedBoolData.Add(b); });
            List<Book> expectedNumData = new();
            model.Books.ForEach(b => { if (b.GenreID == 2) expectedNumData.Add(b); });
            List<Book> expectedStrData = new();
            model.Books.ForEach(b => { if (b.Name == "The Nightingale") expectedStrData.Add(b); });
            List<Book> expectedArrData = new();
            model.Books.ForEach(b => { if (b.Authors.Count() == 2) expectedArrData.Add(b); });
            List<Book> expectedDoubleData = new();
            model.Books.ForEach(b => { if (b.Price > 100) expectedDoubleData.Add(b); });


            var actualBoolData = model.Books.Where(b => b.Available == true);
            var actualNumData = model.Books.Where(b => b.GenreID == 2);
            var actualStrData = model.Books.Where(b => b.Name == "The Nightingale");
            var actualArrData = model.Books.Where(b => b.Authors.Count() == 2);
            var actualDoubleData = model.Books.Where(b => b.Price > 100);

            Assert.Equal(expectedBoolData, actualBoolData);
            Assert.IsType<bool>(actualStrData.First().Available);
            Assert.Equal(expectedNumData, actualNumData);
            Assert.IsType<int>(actualNumData.First().GenreID);
            Assert.Equal(expectedStrData, actualStrData);
            Assert.IsType<string>(actualStrData.First().Name);
            Assert.Equal(expectedArrData, actualArrData);
            Assert.IsType<string[]>(actualArrData.First().Authors);
            Assert.Equal(expectedDoubleData, actualDoubleData);
            Assert.IsType<double>(actualDoubleData.First().Price);
        }
        [Fact]
        public void TestDictionary()
        {
            ModelTest model = new();
            List<Book> expectedData = new();
            int max = 0;
            foreach (var b in model.dictionary.Values)
            {
                if (b.Count > max) expectedData = b;
            }

            var actualData = model.dictionary
                .Where(b => b.Value.Count == model.dictionary.Max(b => b.Value.Count)).Select(g => g.Value).ToList()[0];

            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestSortedList()
        {
            ModelTest model = new();
            var expectedBook = new Book("The Diary of a Young Girl", new string[] { "Anne Frank" }, 004, true, 11, 190.8);

            var actualData = model.sortedList.Contains(new KeyValuePair<int, Book>(11, expectedBook));

            Assert.True(actualData);
        }
        [Fact]
        public void TestQueue()
        {
            ModelTest model = new();
            Queue<Book> expectedData = new();
            foreach(Book b in model.queue)
            {
                if (b.Available) expectedData.Enqueue(b);
            }
            var actualData = model.queue.Where(b => b.Available);
            Assert.Equal(expectedData, actualData);
        }
        [Fact]
        public void TestGroupByOrderBy()
        {
            ModelTest model = new ModelTest();
            var expectedData = model.grouping;
            var actualData = model.Books.GroupBy(b => b.Authors[0]).OrderByDescending(a => a.Count());
            Assert.Equal(expectedData, actualData);
        }
    }
}


