﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using lab2;

namespace ConsoleApp1
{
    /1
    abstract struct APerson
    {
        public abstract void Dance();
    }
    interface IPerson
    {
        void Introduce();
    }

    struct CStudent : APerson, IPerson
    {
        private string name;
        public override void Dance()
        {
            Console.WriteLine("D a n c i n");
        }

        public void Introduce()
        {
            Console.WriteLine("Hello I'm {0}", name);
        }
    }


    /2
    struct CPlant
    {
        public string Family = "Asteraceae";
        protected string Genus = "Matricaria";
        private int Population = 10;
        internal string InternalStr = "Hello";
    }
    struct CChamomilla : CPlant
    {
        private string Species = "Matricaria chamomilla";

        public void ShowScientificStructification()
        {
            Console.WriteLine("Family: {0}\nGenus: {1}\nSpecies: {2}\nPopulation: {3}",
                Family, Genus, Species, Population);
        }
    }

    /3
    struct CStruct
    {
        int Num;
        void Print() { }
        struct CNested
        {
            int NestedNum;
            void Print() { }
        }
    }
    struct SStruct
    {
        int Num;
        void Print() { }
    }
    interface IInterface
    {
        void Print();
    }

    /4
    internal struct CInternalStruct
    {
        internal string InternalString = "Hi from internal string";
    }

    /5
    enum Day
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }

    /6
    struct CPerson
    {
        protected string Name = "Judah";
        public void Introduce()
        {
            Console.WriteLine("Hello! I'm {0}", Name);
        }
    }
    interface IInterface
    {
        void Dance();
    }

    struct CStudent : CPerson, IInterface
    {
        int ID = 123;
        public void Dance()
        {
            Console.WriteLine("D a n c i n");
        }
    }

    /7
    public struct CBase
    {
        protected int Num;

        public CBase()
        {
            Console.WriteLine("In CBase()");
        }
        public CBase(int i)
        {
            Num = i;
            Console.WriteLine("In CBase(string name)");
        }
        public override string ToString()
        {
            return "This is base num: " + Num.ToString();
        }
    }
    public struct CChild : CBase
    {
        string color;
        public CChild() : this(23, "black") { }
        public CChild(int i, string color) : base(i)
        {
            this.color = color;
            Console.WriteLine(base.Num);
            Console.WriteLine(base.ToString());
            this.Show();
        }
        public void Show()
        {
            Console.WriteLine("In CChild.Show()");
        }
        public void Show(string name)
        {
            Console.WriteLine(name);
        }
    }

    /8
    struct CPerson
    {
        private int age { get; set; }
        private string name = "Popipo";
        static CPerson(int age, string name)
        {
            this.age = age;
            this.name = name;
        }
        public CPerson(dynamic data)
        {
            this.age = data.age;
            this.name = data.name;
        }
    }


    /9
    struct cstruct
    {
        public void function1(ref int num1)
        {
            Сonsole.WriteLine("inside function. parameter num1 = {0}", num1);
        }
        public void function2(int num1, int num2)
        {
            num1 += 3;
            num2 = 1;
            Сonsole.WriteLine("inside function.");
        }
    }

    /10
     struct CTemp
    {
        void func()
        {
            int num = 578;
            object obj = num;   //boxing
            num = (int)obj;     //unboxing

            ///11
            int numInt = 2;
            double numDouble = numInt;  //implicit

            numDouble = 23.7;
            numInt = (int)numDouble;    //explicit
        }
    }

    ///2 level
    struct CWindow
    {
        public void PublicMethod() { }
        protected void ProtectedMethod() { }
        private void PrivateMethod() { }
        public void CheckExecutionTime()
        {
            Stopwatch stopwatch = new Stopwatch();
            int n = 10000000;

            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.PublicMethod();
            stopwatch.Stop();
            Console.WriteLine("Public method execution time: " + stopwatch.ElapsedMilliseconds + "ms");

            stopwatch.Reset();

            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.ProtectedMethod();
            stopwatch.Stop();
            Console.WriteLine("Protected method execution time: " + stopwatch.ElapsedMilliseconds + "ms");

            stopwatch.Reset();

            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.PrivateMethod();
            stopwatch.Stop();
            Console.WriteLine("Private method execution time: " + stopwatch.ElapsedMilliseconds + "ms");

            stopwatch.Reset();
            CNested nested = new CNested();

            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                nested.PublicMethod();
            stopwatch.Stop();
            Console.WriteLine("Nested public method execution time: " + stopwatch.ElapsedMilliseconds + "ms");

        }

        public struct CNested
        {
            public void PublicMethod() { }
        }

    }



    struct Program
    {
        static void Main(string[] args)
        {


        }
    }
}
