﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace lab2
{
    ///1
    //abstract class APerson
    //{
    //    public abstract void Dance();
    //}
    //interface IPerson
    //{
    //    void Introduce();
    //}

    //class CStudent : APerson, IPerson
    //{
    //    private string name;
    //    public override void Dance()
    //    {
    //        Console.WriteLine("D a n c i n");
    //    }

    //    public void Introduce()
    //    {
    //        Console.WriteLine("Hello I'm {0}", name);
    //    }
    //}


    ///2
    //class CPlant
    //{
    //    public string Family = "Asteraceae";
    //    protected string Genus = "Matricaria";
    //    private int Population = 10;
    //    internal string InternalStr = "Hello";
    //}
    //class CChamomilla : CPlant
    //{
    //    private string Species = "Matricaria chamomilla";

    //    public void ShowScientificClassification()
    //    {
    //        Console.WriteLine("Family: {0}\nGenus: {1}\nSpecies: {2}\nPopulation: {3}",
    //            Family, Genus, Species, Population);
    //    }
    //}

    ///3
    //class CClass
    //{
    //    int Num;
    //    void Print() { }
    //    class CNested
    //    {
    //        int NestedNum;
    //        void Print() { }
    //    }
    //}
    //struct SStruct
    //{
    //    int Num;
    //    void Print() { }
    //}
    //interface IInterface
    //{
    //    void Print();
    //}

    ///4
    //internal class CInternalClass
    //{
    //    internal string InternalString = "Hi from internal string";
    //}

    ///5
    //enum Day
    //{
    //    Monday,
    //    Tuesday,
    //    Wednesday,
    //    Thursday,
    //    Friday,
    //    Saturday,
    //    Sunday
    //}

    ///6
    //class CPerson
    //{
    //    protected string Name = "Judah";
    //    public void Introduce()
    //    {
    //        Console.WriteLine("Hello! I'm {0}", Name);
    //    }
    //}
    //interface IInterface
    //{
    //    void Dance();
    //}

    //class CStudent : CPerson, IInterface
    //{
    //    int ID = 123;
    //    public void Dance()
    //    {
    //        Console.WriteLine("D a n c i n");
    //    }
    //}

    ///7
    //public class CBase
    //{
    //    protected int Num = 10;

    //    public CBase()
    //    {
    //        Console.WriteLine("In CBase()");
    //    }
    //    public CBase(int i)
    //    {
    //        Num = i;
    //        Console.WriteLine("In CBase(string name)");
    //    }
    //    public override string ToString()
    //    {
    //        return "This is base num: " + Num.ToString();
    //    }
    //    public virtual void Show()
    //    {
    //        Console.WriteLine("In method Show of base");
    //    }
    //}
    //public class CChild : CBase
    //{
    //    string color;
    //    int[] arr;
    //    public CChild() : this(23, "black") { }
    //    public CChild(int i, string color) : base(i)
    //    {
    //        this.color = color;
    //        Console.WriteLine(base.Num);
    //        Console.WriteLine(base.ToString());
    //        base.Show();
    //        this.Show();
    //        this.Show(this);
    //    }
    //    public override void Show()
    //    {
    //        Console.WriteLine("In CChild.Show()");
    //    }
    //    public void Show(CChild obj)
    //    {
    //        Console.WriteLine(obj.ToString());
    //    }
    //    public int this[int i]
    //    {
    //        get { return arr[i]; }
    //        set { arr[i] = value; }
    //    }
    //}

    ///8
    //class CPerson
    //{
    //    private int age;
    //    private string name = "NoName";
    //    static CPerson()
    //    {
    //        Console.WriteLine("In static constructor");
    //    }
    //    public CPerson(int age, string name)
    //    {
    //        this.age = age;
    //        this.name = name;
    //    }
    //}


    ///9
    //class CClass
    //{
    //    public void function1(ref int num1)
    //    {
    //        Console.WriteLine("Inside function. Parameter num1 = {0}", num1);
    //    }
    //    public void function2(int num1, int num2)
    //    {
    //        num1 += 3;
    //        num2 = 1;
    //        Console.WriteLine("Inside function.");
    //    }
    //}

    ///10
    //class CTemp
    //{
    //    void func()
    //    {
    //        int num = 578;
    //        object obj = num;   //boxing
    //        num = (int)obj;     //unboxing

    //        ///11
    //        int numInt = 2;
    //        double numDouble = numInt;  //implicit

    //        numDouble = 23.7;
    //        numInt = (int)numDouble;    //explicit
    //    }
    //}

    ///2 level
    class CWindow
    {
        int num = 1;
        public void PublicMethod() { num++; }
        protected void ProtectedMethod() { num++; }
        private void PrivateMethod() { num++; }
        internal void InternalMethod() { num++; }
        protected internal void InternalProtectedMethod() { num++; }
        private protected void PrivateProtectedMethod() { num++; }
        public void CheckExecutionTime()
        {
            Stopwatch stopwatch = new Stopwatch();
            int n = 10000000;
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.PublicMethod();
            stopwatch.Stop();
            Console.WriteLine("Public method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.ProtectedMethod();
            stopwatch.Stop();
            Console.WriteLine("Protected method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.PrivateMethod();
            stopwatch.Stop();
            Console.WriteLine("Private method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.InternalMethod();
            stopwatch.Stop();
            Console.WriteLine("Internal method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.PrivateProtectedMethod();
            stopwatch.Stop();
            Console.WriteLine("Private-protected method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                this.InternalProtectedMethod();
            stopwatch.Stop();
            Console.WriteLine("Protected-internal method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
            CNested nested = new CNested();
            stopwatch.Start();
            for (int i = 0; i < n; ++i)
                nested.PublicMethod(num);
            stopwatch.Stop();
            Console.WriteLine("Nested public method execution time: " + stopwatch.ElapsedMilliseconds + "ms");
        }

        public class CNested
        {
            public void PublicMethod(int num) { num++; }
        }

    }

    //class CPerson
    // {
    //     string name;
    //     int id;
    //     public override string ToString()
    //     {
    //         return "name: " + name + "\nid: " + id;
    //     }
    //     public override bool Equals(object obj)
    //     {
    //         var temp = obj as CPerson;
    //         if (temp == null) return false;
    //         return this.name.Equals(temp.name) && this.id.Equals(temp.id);
    //     }
    //     public override int GetHashCode()
    //     {
    //         return this.id.GetHashCode();
    //     }
    //     protected override CPerson MemberwiseClone()
    //     {

    //     }
    // }


    class Program
    {
        static void Main(string[] args)
        {
            CWindow obj = new CWindow();
            obj.CheckExecutionTime();
        }
    }
}
