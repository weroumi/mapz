using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Potions;

public class AppleObject : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Inventory.GetInstance().AppleCount++;
        StatusManager.instance.GetApple();
        Destroy(gameObject);
    }
}
