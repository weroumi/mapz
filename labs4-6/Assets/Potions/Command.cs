﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Potions
{
    public interface ICommand
    {
        void Execute();
        void Undo();
    }
    class TakeBluePotion : ICommand
    {
        public float Strength;
        public TakeBluePotion(float strength)
        {
            this.Strength = strength;
        }
        public void Execute()
        {
            StatusManager.instance.speed += Strength;
        }
        public void Undo()
        {
            StatusManager.instance.speed -= Strength;
            Inventory.GetInstance().AddBluePotion();
        }
    }
    class TakeYellowPotion : ICommand
    {
        public float Strength;
        public TakeYellowPotion(float strength)
        {
            this.Strength = strength;
        }
        public void Execute()
        {
            StatusManager.instance.strength += (int)Strength;
        }
        public void Undo()
        {
            StatusManager.instance.strength -= (int)Strength;
            Inventory.GetInstance().AddYellowPotion();
        }
    }
    class TakeRedPotion : ICommand
    {
        public float Strength;
        public TakeRedPotion(float strength)
        {
            this.Strength = strength;
        }
        public void Execute()
        {
            StatusManager.instance.health += (int)Strength;
        }
        public void Undo()
        {
            StatusManager.instance.health -= (int)Strength;
            Inventory.GetInstance().AddRedPotion();
        }
    }
    class Invoker 
    {
        private ICommand onStart;
        private ICommand onFinish;
        public void SetOnStart(ICommand command)
        {
            this.onStart = command;
        }
        public void SetOnFinish(ICommand command)
        {
            this.onFinish = command;
        }

    }
}
