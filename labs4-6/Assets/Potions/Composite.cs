﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Potions
{
    public abstract class PotionSystemComponent
    {
        public string Name { get; }
        public int Count { get; set; }
        public PotionSystemComponent(string name) 
        {
            this.Name = name;
        }
        public abstract float GetStrength();
        public void AddComponent() { Count++; }
        public void RemoveComponent() { Count--; }
    }
    public class PotionComponent : PotionSystemComponent
    {
        public float Strength { get; }
        public PotionComponent(string name, float strength) : base(name)
        {
            this.Strength = strength;
        }
        public override float GetStrength()
        {
            return Strength;
        }
    }
    class CraftingComposite : PotionSystemComponent
    {
        public List<PotionSystemComponent> children { get; } = new();
        public CraftingComposite(string name) : base(name) { }
        public override float GetStrength()
        {
            return this.children.Sum(x => x.GetStrength());
        }
        public void Add(PotionSystemComponent newNode)
        {
            this.children.Add(newNode);
        }
        public void Remove(PotionSystemComponent deleteNode)
        {
            this.children.Remove(deleteNode);
        }
    }
}
