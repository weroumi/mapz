﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Potions
{
    public abstract class IPart
    {
        protected string recipe;
        protected float strength;
        public float Strength
        {
            get { return strength; }
        }
        public override bool Equals(object obj)
        {
            IPart o = (IPart)obj;
            return o.recipe == this.recipe && o.strength == this.strength;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public abstract class CopyOfIPart
    {
        protected string recipe;
        protected float strength;
        public float Strength
        {
            get { return strength; }
        }
        public override bool Equals(object obj)
        {
            CopyOfIPart o = (CopyOfIPart)obj;
            return o.recipe == this.recipe && o.strength == this.strength;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
    class MonsterEgg : IPart
    {
        public MonsterEgg()
        {
            recipe = "Blue Energy Potion";
            strength = 2;
        }
    }
    class MonsterEye : IPart
    {
        public MonsterEye()
        {
            recipe = "Yellow Strength Potion";
            strength = 2;
        }
    }
    class Mushroom : IPart
    {
        public Mushroom()
        {
            recipe = "Blue Energy Potion";
            strength = 1;
        }
    }
    class Cheese : IPart
    {
        public Cheese()
        {
            recipe = "Yellow Strength Potion";
            strength = 1;
        }
    }
    class Apple : IPart
    {
        public Apple()
        {
            recipe = "Red Health Potion";
            strength = 1;
        }
    }
    class Ruby : IPart
    {
        public Ruby()
        {
            recipe = "Red Health Potion";
            strength = 2;
        }
    }
}
