﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Potions
{
    public interface IBuilder
    {
        void AddMonsterEgg();
        void AddMonsterEye();
        void AddMushroom();
        void AddCheese();
        void AddApple();
        void AddRuby();
    }
    public class CreatePotion : IBuilder
    {
        private Potion potion = new Potion();
        public CreatePotion()
        {
            this.Reset();
        }

        public void AddApple()
        {
            this.potion.Add(new Apple());
        }

        public void AddCheese()
        {
            this.potion.Add(new Cheese());
        }

        public void AddMonsterEgg()
        {
            this.potion.Add(new MonsterEgg());
        }

        public void AddMonsterEye()
        {
            this.potion.Add(new MonsterEye());
        }

        public void AddMushroom()
        {
            this.potion.Add(new Mushroom());
        }

        public void AddRuby()
        {
            this.potion.Add(new Ruby());
        }
        public void Reset()
        {
            this.potion = new Potion();
        }
        public Potion GetPotion()
        {
            Potion result = this.potion;
            this.Reset();
            return result;
        }
        
    }
    public interface IWeapon
    {
        float Strength { get; set; }
    }
    public class Potion : IWeapon
    {
        private List<IPart> parts = new List<IPart>();
        public string Name { get; set; }
        public float Strength { get; set; }
        public Potion()
        {
        }
        public void Add(IPart part)
        {
            this.parts.Add(part);
            Strength += part.Strength;
        }
    }
    public class Director
    {
        private IBuilder builder;
        public IBuilder Builder
        {
            set { builder = value; }
        }
        public static bool BluePotionAvailable;
        public static bool YellowPotionAvailable;
        public static bool RedPotionAvailable;
        public void BuildBluePotion()
        {
            if (BluePotionAvailable)
            {
                this.builder.AddMonsterEgg();
                this.builder.AddMushroom();
            }
        }
        public void BuildYellowPotion()
        {
            if (YellowPotionAvailable)
            {
                this.builder.AddCheese();
                this.builder.AddMonsterEye();
            }
        }
        public void BuildRedPotion()
        {
            if (RedPotionAvailable)
            {
                this.builder.AddApple();
                this.builder.AddRuby();
            }
        }
    }
}

