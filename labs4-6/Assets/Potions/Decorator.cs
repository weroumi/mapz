﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Potions
{
    public abstract class WeaponUpgrade : IWeapon
    {
        protected IWeapon weapon;
        public WeaponUpgrade(IWeapon weapon)
        {
            this.weapon = weapon;
        }
        public virtual float Strength
        {
            get
            {
                return this.weapon.Strength;
            }
            set { }
        }
    }
    //puts a spell and makes potion 2 times stronger
    public class SpellPotionDecorator : WeaponUpgrade
    {
        public SpellPotionDecorator(IWeapon weapon) : base(weapon) { }
        public override float Strength { get => base.Strength * 2; set => base.Strength = value; }
    }
    // if you use the potion at the midnight it will be 5 units stronger
    public class MidnightPotionDecorator : WeaponUpgrade
    {
        public MidnightPotionDecorator(IWeapon weapon) : base(weapon) { }
        public override float Strength { get => base.Strength + 5; set => base.Strength = value; }
    }
}
