﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;

namespace Assets.Potions
{
    public class Inventory
    {
        private static Inventory instance;
        private Inventory()
        {
            //BlueEnergyPotions = new();
            //YellowStrengthPotion = new();
            //RedHealthPotion = new();
            director.Builder = builder;
            BlueEnergyPotion.Add(new PotionComponent("Mushroom", 1));
            BlueEnergyPotion.Add(new PotionComponent("MonsterEgg", 2));
            YellowStrengthPotion.Add(new PotionComponent("Cheese", 1));
            YellowStrengthPotion.Add(new PotionComponent("MonsterEye", 2));
            RedHealthPotion.Add(new PotionComponent("Apple", 1));
            RedHealthPotion.Add(new PotionComponent("Ruby", 2));
            items.Add(BlueEnergyPotion);
            items.Add(YellowStrengthPotion);
            items.Add(RedHealthPotion);
        }
        public static Inventory GetInstance()
        {
            if (instance == null)
            {
                instance = new Inventory();
            }
            return instance;
        }
        CraftingComposite items = new CraftingComposite("Items");
        private CraftingComposite BlueEnergyPotion = new CraftingComposite("Blue");
        private CraftingComposite YellowStrengthPotion = new CraftingComposite("Yellow");
        private CraftingComposite RedHealthPotion = new CraftingComposite("Red");
        //private List<IWeapon> BlueEnergyPotions;
        //private List<IWeapon> YellowStrengthPotion;
        //private List<IWeapon> RedHealthPotion;
        public int MonsterEyeCount 
        { 
            get 
            { 
                return YellowStrengthPotion.children.Where(ch => ch.Name.Equals("MonsterEye")).First().Count;
            }
            set
            {
                YellowStrengthPotion.children.Where(ch => ch.Name.Equals("MonsterEye")).First().Count += value;
            }
        }
        public int MonsterEggCount 
        {
            get
            {
                return BlueEnergyPotion.children.Where(ch => ch.Name.Equals("MonsterEgg")).First().Count;
            }
            set
            {
                BlueEnergyPotion.children.Where(ch => ch.Name.Equals("MonsterEgg")).First().Count += value;
            }
        }
        public int RubyCount 
        {
            get
            {
                return RedHealthPotion.children.Where(ch => ch.Name.Equals("Ruby")).First().Count;
            }
            set
            {
                RedHealthPotion.children.Where(ch => ch.Name.Equals("Ruby")).First().Count += value;
            }
        }
        public int MushroomCount
        {
            get
            {
                return BlueEnergyPotion.children.Where(ch => ch.Name.Equals("Mushroom")).First().Count;
            }
            set
            {
                BlueEnergyPotion.children.Where(ch => ch.Name.Equals("Mushroom")).First().Count += value;
            }
        }
        public int CheeseCount 
        {
            get 
            {
                return YellowStrengthPotion.children.Where(ch => ch.Name.Equals("Cheese")).First().Count;
            }
            set
            {
                YellowStrengthPotion.children.Where(ch => ch.Name.Equals("Cheese")).First().Count += value;
            } 
        }
        public int AppleCount 
        {
            get
            {
                return RedHealthPotion.children.Where(ch => ch.Name.Equals("Apple")).First().Count;
            }
            set
            {
                RedHealthPotion.children.Where(ch => ch.Name.Equals("Apple")).First().Count += value;
            }
        }
        public int BluePotionCount { get { return BlueEnergyPotion.Count; } }
        public int YellowPotionCount { get { return YellowStrengthPotion.Count; } }
        public int RedPotionCount { get { return RedHealthPotion.Count; } }

        Director director = new Director();
        CreatePotion builder = new CreatePotion();

        public Stack<ICommand> commands = new();
        public void AddBluePotion()
        {
            //if (MushroomCount <= 0 || MonsterEggCount <= 0)
            //    return;
            //director.BuildBluePotion();
            //BlueEnergyPotions.Add(builder.GetPotion());
            //MushroomCount--;
            //MonsterEggCount--;
            if (BlueEnergyPotion.children.Where(ch => ch.Name.Equals("MonsterEgg")).First().Count <= 0 ||
                BlueEnergyPotion.children.Where(ch => ch.Name.Equals("Mushroom")).First().Count <= 0)
            {
                BlueEnergyPotion.children.Where(ch => ch.Name.Equals("MonsterEgg")).First().Count = 0;
                BlueEnergyPotion.children.Where(ch => ch.Name.Equals("Mushroom")).First().Count = 0;
            }
                
            BlueEnergyPotion.Count++;
            BlueEnergyPotion.children.Where(ch => ch.Name.Equals("MonsterEgg")).First().Count--;
            BlueEnergyPotion.children.Where(ch => ch.Name.Equals("Mushroom")).First().Count--;
        }
        public void AddYellowPotion()
        {
            //if (CheeseCount <= 0 || MonsterEyeCount <= 0)
            //    return;
            //director.BuildYellowPotion();
            //YellowStrengthPotion.Add(builder.GetPotion());
            //CheeseCount--;
            //MonsterEyeCount--;
            if (YellowStrengthPotion.children.Where(ch => ch.Name.Equals("MonsterEye")).First().Count <= 0 ||
                YellowStrengthPotion.children.Where(ch => ch.Name.Equals("Cheese")).First().Count <= 0)
            {
                YellowStrengthPotion.children.Where(ch => ch.Name.Equals("MonsterEye")).First().Count = 0;
                YellowStrengthPotion.children.Where(ch => ch.Name.Equals("Cheese")).First().Count = 0;
            }

            YellowStrengthPotion.Count++;
            YellowStrengthPotion.children.Where(ch => ch.Name.Equals("MonsterEye")).First().Count--;
            YellowStrengthPotion.children.Where(ch => ch.Name.Equals("Cheese")).First().Count--;
        }
        public void AddRedPotion()
        {
            //if (RubyCount <= 0 || AppleCount <= 0)
            //    return;
            //director.BuildRedPotion();
            //RedHealthPotion.Add(builder.GetPotion());
            //RubyCount--;
            //AppleCount--;
            if (RedHealthPotion.children.Where(ch => ch.Name.Equals("Ruby")).First().Count <= 0 ||
                RedHealthPotion.children.Where(ch => ch.Name.Equals("Apple")).First().Count <= 0)
            {
                RedHealthPotion.children.Where(ch => ch.Name.Equals("Ruby")).First().Count = 0;
                RedHealthPotion.children.Where(ch => ch.Name.Equals("Apple")).First().Count = 0;
            }
            RedHealthPotion.Count++;
            RedHealthPotion.children.Where(ch => ch.Name.Equals("Ruby")).First().Count--;
            RedHealthPotion.children.Where(ch => ch.Name.Equals("Apple")).First().Count--;
        }
        public void AddRecipe(string recipe)
        {
            if(recipe.Equals("Blue Energy Potion"))
            {
                Director.BluePotionAvailable = true;
            }
            else if(recipe.Equals("Yellow Strength Potion"))
            {
                Director.YellowPotionAvailable = true;
            }
            else if(recipe.Equals("Red Health Potion"))
            {
                Director.RedPotionAvailable = true;
            }
        }
        public void TakeBluePotion()
        { 
            if (BlueEnergyPotion.Count > 0)
            {
                //BlueEnergyPotions[0] = DecoratePotion(BlueEnergyPotions[0]);
                //StatusManager.instance.speed += BlueEnergyPotions[0].Strength;
                //BlueEnergyPotions.RemoveAt(0);

                director.BuildBluePotion();
                //StatusManager.instance.speed += DecoratePotion(builder.GetPotion()).Strength;
                TakeBluePotion command = new(DecoratePotion(builder.GetPotion()).Strength);
                command.Execute();
                commands.Push(command);
                BlueEnergyPotion.Count--;
            }
        }
        public void TakeYellowPotion()
        {
            if (YellowStrengthPotion.Count > 0)
            {
                //YellowStrengthPotion[0] = DecoratePotion(YellowStrengthPotion[0]);
                //StatusManager.instance.strength += (int)YellowStrengthPotion[0].Strength;
                //YellowStrengthPotion.RemoveAt(0);
                director.BuildYellowPotion();
                //StatusManager.instance.strength += (int)DecoratePotion(builder.GetPotion()).Strength;
                TakeYellowPotion command = new(DecoratePotion(builder.GetPotion()).Strength);
                command.Execute();
                commands.Push(command);
                YellowStrengthPotion.Count--;
            }
        }
        public void TakeRedPotion()
        {
            if (RedHealthPotion.Count > 0)
            {
                //RedHealthPotion[0] = DecoratePotion(RedHealthPotion[0]);
                //StatusManager.instance.health += (int)RedHealthPotion[0].Strength;
                //RedHealthPotion.RemoveAt(0);
                director.BuildRedPotion();
                //StatusManager.instance.health += (int)DecoratePotion(builder.GetPotion()).Strength;
                TakeRedPotion command = new(DecoratePotion(builder.GetPotion()).Strength);
                command.Execute();
                commands.Push(command);
                RedHealthPotion.Count--;
            }
        }
        IWeapon DecoratePotion(IWeapon potion)
        {
            System.Random rnd = new System.Random();
            int num = rnd.Next();
            if (num % 5 == 0)
            {
                GameObject.Find("TxtSpell").GetComponent<TextMeshProUGUI>().text = "The spell has been put on the potion. It's x2 stronger.";
                potion = new SpellPotionDecorator(potion);
            }
            if (DateTime.Now.Hour == 0)
            {
                GameObject.Find("TxtMidnight").GetComponent<TextMeshProUGUI>().text = "It's midnight, the potion is 5 units stronger.";
                potion = new MidnightPotionDecorator(potion);
            }
            return potion;
        }
    }
}