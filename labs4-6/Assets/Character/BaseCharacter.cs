using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacter 
{
    private string name;
    // stats
    private int stamina;
    private float endurance;
    private int strength;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    public int Stamina
    {
        get { return stamina; }
        set { stamina = value; }
    }
    public float Endurance
    {
        get { return endurance; }
        set { endurance = value; }
    }
    public int Strength
    {
        get { return strength; }
        set { strength = value; }
    }
}

