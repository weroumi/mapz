using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Character;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    private Rigidbody2D rb;
    private Vector2 movementDirection;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        movementSpeed = StatusManager.instance.speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            movementSpeed += 0.2f ;
            StatusManager.instance.TransitionTo(new Run());
        }
        else if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.UpArrow))
        {
            movementSpeed = StatusManager.instance.speed;
            StatusManager.instance.TransitionTo(new Walk());
        }
        movementDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }
    private void FixedUpdate()
    {
        rb.velocity = movementDirection * movementSpeed;
    }

}
