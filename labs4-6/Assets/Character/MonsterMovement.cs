using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Potions;
using Assets.Character;

public class MonsterMovement : MonoBehaviour
{
    Rigidbody2D rb;
    GameObject target;
    float moveSpeed;
    Vector2 directionToTarget;
    public GameObject explosion;
    private IStrategy _strategy;
    // Start is called before the first frame update
    void Start()
    {
        StatusManager.instance.currentHealth = StatusManager.instance.health;
        _strategy = new ClickFight();
        target = GameObject.Find("Player(Clone)");
        rb = GetComponent<Rigidbody2D>();
        moveSpeed = (float)SpawnMonster.facade.monsterStatus.Endurance;
    }

    // Update is called once per frame
    void Update()
    {
        MoveMonster();
        if (Input.GetKey(KeyCode.J))
        {
            new EnterDefend().Fight();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        int impactForce = SpawnMonster.facade.monsterStatus.Strength * -1;
        StatusManager.instance.EditHealth(impactForce);
    }
    private void OnMouseDown()
    {
        SpawnMonster.facade.monsterStatus.Stamina -= StatusManager.instance.strength;
        if(SpawnMonster.facade.monsterStatus.Stamina <= 0)
        {
            if (SpawnMonster.facade.Reward == "Egg") Inventory.GetInstance().MonsterEggCount++;
            else if (SpawnMonster.facade.Reward == "Eye") Inventory.GetInstance().MonsterEyeCount++;
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(directionToTarget.x, directionToTarget.y) * moveSpeed;

    }
    void MoveMonster()
    {
        directionToTarget = (target.transform.position - transform.position).normalized;
    }
}
