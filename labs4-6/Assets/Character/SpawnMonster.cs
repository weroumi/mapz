//using System.Collections;
//using System.Collections.Generic;
//using System;
using UnityEngine;
using Assets.Character;
using UnityEngine.UI;
using TMPro;

public class SpawnMonster : MonoBehaviour
{
    [SerializeField]
    private GameObject monster;
    public static MonsterFacade facade;

    // Start is called before the first frame update
    void Start()
    {
        facade = new(monster);
        facade.SpawnStart(monster, transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
