﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Character
{
    public interface IStrategy
    {
        void Fight();
    }
    class EnterDefend : IStrategy
    {
        public void Fight()
        {
            StatusManager.instance.health = StatusManager.instance.currentHealth;
        }
    }
    class ClickFight : IStrategy
    {
        public void Fight()
        {
            SpawnMonster.facade.monsterStatus.Stamina -= StatusManager.instance.strength;
        }
    }
}
