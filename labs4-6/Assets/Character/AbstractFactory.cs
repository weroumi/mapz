﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Character
{
    enum MonsterType
    {
        EASY, HARD
    }
    interface ICreateMonster
    {
        IMonsterEye CreateMonsterEye();
        IMonsterEgg CreateMonsterEgg();
    }
    class CreateMonsterEasy : ICreateMonster
    {
        public IMonsterEye CreateMonsterEye()
        {
            return new MonsterEyeEasy();
        }
        public IMonsterEgg CreateMonsterEgg()
        {
            return new MonsterEggEasy();
        }
    }
    class CreateMonsterHard : ICreateMonster
    {
        public IMonsterEye CreateMonsterEye()
        {
            return new MonsterEyeHard();
        }
        public IMonsterEgg CreateMonsterEgg()
        {
            return new MonsterEggHard();
        }
    }
    abstract class IMonsterEye : BaseCharacter
    {
        public string Award { get{ return "Eye"; } }
    }
    class MonsterEyeEasy : IMonsterEye
    {
        public MonsterEyeEasy()
        {
            Name = "Monster Eye Easy";
            Stamina = 8;
            Endurance = 2f;
            Strength = 1;
        }
    }
    class MonsterEyeHard : IMonsterEye
    {
        public MonsterEyeHard()
        {
            Name = "Monster Eye Hard";
            Stamina = 20;
            Endurance = 5f;
            Strength = 3;
        }
    }
    abstract class IMonsterEgg : BaseCharacter
    {
        public string Award { get { return "Egg"; } }
    }
    class MonsterEggEasy : IMonsterEgg
    {
        public MonsterEggEasy()
        {
            Name = "Monster Egg Easy";
            Stamina = 8;
            Endurance = 2f;
            Strength = 1;
        }
    }
    class MonsterEggHard : IMonsterEgg
    {
        public MonsterEggHard()
        {
            Name = "Monster Egg Hard";
            Stamina = 20;
            Endurance = 5f;
            Strength = 3;
        }
    }
}

