using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Potions;
using TMPro;

public class StatueManagement : MonoBehaviour
{
    int stamina;
    // Start is called before the first frame update
    void Start()
    {
        stamina = 10;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        stamina -= StatusManager.instance.strength;
        if (stamina <= 0 && !Director.BluePotionAvailable)
        {
            Destroy(gameObject);
            Inventory.GetInstance().AddRecipe("Blue Energy Potion");
            GameObject.Find("TxtMessage").GetComponent<TextMeshProUGUI>().text = "Congrats! Now Blue Potion is available.";
        }
    }
}
