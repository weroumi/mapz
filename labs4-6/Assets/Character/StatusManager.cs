using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Assets.Potions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Character;

public class StatusManager : MonoBehaviour
{
    public int health;
    public float speed;
    public int strength;
    private State _state = null;
    public static StatusManager instance;
    public int currentHealth;
    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        SetUI();
        DontDestroyOnLoad(gameObject);
    }
    
    public void TransitionTo(State state)
    {
        this._state = state;
    }
    public void GetApple()
    {
        this._state.CollectApple();
    }
    public void GetMushroom()
    {
        this._state.CollectMushroom();
    }
    public void GetCheese()
    {
        this._state.CollectCheese();
    }
    public void GetRuby()
    {
        this._state.CollectRuby();
    }
    public void EditHealth(int value)
    {
        health += value;
        SetUI();
    }
    public void EditSpeed(float value)
    {
        speed += value;
        SetUI();
    }
    public void EditPower(int value)
    {
        strength += value;
        SetUI();
    }
    public void SetUI()
    {
        GameObject.Find("txtHealth").GetComponent<TextMeshProUGUI>().text = health.ToString();
        GameObject.Find("txtSpeed").GetComponent<TextMeshProUGUI>().text = speed.ToString();
        GameObject.Find("txtStrength").GetComponent<TextMeshProUGUI>().text = strength.ToString();
    }
    public void StartMix()
    {
        var cbBlue = GameObject.Find("ChbBlue").GetComponent<Toggle>();
        var cbYellow = GameObject.Find("ChbYellow").GetComponent<Toggle>();
        var cbRed = GameObject.Find("ChbRed").GetComponent<Toggle>();
        if (Director.BluePotionAvailable)
            cbBlue.interactable = true;
        if (Director.YellowPotionAvailable)
            cbYellow.interactable = true;
        if (Director.RedPotionAvailable)
            cbRed.interactable = true;
        SetInventoryUI();
        GameObject.Find("BtnToScene").GetComponent<Button>().onClick.AddListener(delegate { SceneManager.LoadScene("SampleScene"); });
        GameObject.Find("BtnMix").GetComponent<Button>().onClick.AddListener(delegate {
            if (cbBlue.isOn)
            {
                Inventory.GetInstance().AddBluePotion();
            }
            if (cbYellow.isOn)
            {
                Inventory.GetInstance().AddYellowPotion();
            }
            if (cbRed.isOn)
            {
                Inventory.GetInstance().AddRedPotion();
            }
            SetInventoryUI();
        });
        GameObject.Find("BtnUseBluePotion").GetComponent<Button>().onClick.AddListener(delegate
        {
            Inventory.GetInstance().TakeBluePotion();
            SetInventoryUI();
        });
        GameObject.Find("BtnUseYellowPotion").GetComponent<Button>().onClick.AddListener(delegate
        {
            Inventory.GetInstance().TakeYellowPotion();
            SetInventoryUI();
        });
        GameObject.Find("BtnUseRedPotion").GetComponent<Button>().onClick.AddListener(delegate
        {
            Inventory.GetInstance().TakeRedPotion();
            SetInventoryUI();
        });
        GameObject.Find("BtnUndo").GetComponent<Button>().onClick.AddListener(delegate
        {
            if(Inventory.GetInstance().commands.Count <= 0)
            {
                return;
            }
            Inventory.GetInstance().commands.Pop().Undo();
            SetInventoryUI();
        });
    }
    void SetInventoryUI()
    {
        StatusManager.instance.SetUI();
        GameObject.Find("AppleCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().AppleCount.ToString();
        GameObject.Find("CheeseCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().CheeseCount.ToString();
        GameObject.Find("MushroomCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().MushroomCount.ToString();
        GameObject.Find("MonsterEggCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().MonsterEggCount.ToString();
        GameObject.Find("MonsterEyeCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().MonsterEyeCount.ToString();
        GameObject.Find("RubyCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().RubyCount.ToString();
        GameObject.Find("TxtEnergyPotionCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().BluePotionCount.ToString();
        GameObject.Find("TxtPowerPotionCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().YellowPotionCount.ToString();
        GameObject.Find("TxtHealthPotionCount").GetComponent<TextMeshProUGUI>().text = Inventory.GetInstance().RedPotionCount.ToString();
        if (Inventory.GetInstance().MonsterEggCount <= 0 || Inventory.GetInstance().MushroomCount <= 0)
        {
            GameObject.Find("ChbBlue").GetComponent<Toggle>().interactable = false;
        }
        if (Inventory.GetInstance().MonsterEyeCount <= 0 || Inventory.GetInstance().CheeseCount <= 0)
        {
            GameObject.Find("ChbYellow").GetComponent<Toggle>().interactable = false;
        }
        if (Inventory.GetInstance().AppleCount <= 0 || Inventory.GetInstance().RubyCount <= 0)
        {
            GameObject.Find("ChbRed").GetComponent<Toggle>().interactable = false;
        }
    }
    public void SpawnPlayerStart()
    {
        if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
            GameObject.Find("BtnMix").GetComponent<Button>().onClick.AddListener(delegate { SceneManager.LoadScene("Mix"); });
        if (SceneManager.GetActiveScene().name.Equals("Visit"))
            SetVisitUI();
    }
    void SetVisitUI()
    {
        var dropdown = GameObject.Find("Recipe").GetComponent<TMP_Dropdown>();
        if (Director.YellowPotionAvailable)
        {
            TMP_Dropdown.OptionData option = dropdown.options.Find(o => string.Equals(o.text, "Yellow Strength Potion"));
            dropdown.options.Remove(option);
        }
        if (Director.RedPotionAvailable)
        {
            TMP_Dropdown.OptionData option = dropdown.options.Find(o => string.Equals(o.text, "Red Health Potion"));
            dropdown.options.Remove(option);
        }
        if (Director.RedPotionAvailable && Director.YellowPotionAvailable)
        {
            Destroy(dropdown);
        }
        dropdown.onValueChanged.AddListener(delegate {
            var btnOk = GameObject.Find("BtnOk").GetComponent<Button>();
            var btnDone = GameObject.Find("BtnDone").GetComponent<Button>();
            var txtTask = GameObject.Find("TxtTask").GetComponent<TextMeshProUGUI>();
            if (dropdown.options[dropdown.value].text.Equals("Yellow Strength Potion"))
            {
                txtTask.text = "To get this recipe you have to collect a Monster Eye from fight with monster in Forest.";
                btnOk.interactable = true;
                btnDone.interactable = true;
                btnOk.onClick.AddListener(delegate
                {
                    txtTask.text = "Good Luck!";
                    btnDone.interactable = false;
                    btnOk.interactable = false;
                });
                btnDone.onClick.AddListener(delegate
                {
                    btnDone.interactable = false;
                    btnOk.interactable = false;
                    if (Inventory.GetInstance().MonsterEyeCount >= 1)
                    {
                        Inventory.GetInstance().AddRecipe("Yellow Strength Potion");
                        txtTask.text = "Congrats, bruh! Now this recipe is available.";
                        TMP_Dropdown.OptionData option = dropdown.options.Find(o => string.Equals(o.text, "Yellow Strength Potion"));
                        dropdown.options.Remove(option);
                    }
                    else
                    {
                        txtTask.text = "Oh dude you better not trick me. I won't forgive this, now you lose points of health.";
                        StatusManager.instance.health = StatusManager.instance.health - 5;
                    }
                });
            }
            else if (dropdown.options[dropdown.value].text.Equals("Red Health Potion"))
            {
                txtTask.text = "To get this recipe you have to solve my riddle.";
                btnOk.interactable = true;
                btnOk.onClick.AddListener(delegate
                {
                    txtTask.text = "What is stronger than steel but can�t handle the sun?";
                    string answer = "Ice";
                    btnOk.interactable = false;
                    var input = GameObject.Find("InpAnswer").GetComponent<TMP_InputField>();
                    input.interactable = true;
                    input.onEndEdit.AddListener(delegate
                    {
                        if (input.text.ToLower().Equals(answer.ToLower()))
                        {
                            Inventory.GetInstance().AddRecipe("Red Health Potion");
                            txtTask.text = "Wow! Slay, now this recipe is available.";
                            TMP_Dropdown.OptionData option = dropdown.options.Find(o => string.Equals(o.text, "Red Health Potion"));
                            dropdown.options.Remove(option);
                        }
                        else
                        {
                            txtTask.text = "Hehe! It's tricky, try again.";
                            btnOk.interactable = true;
                        }
                    });
                });
            }
        });
    }

}
