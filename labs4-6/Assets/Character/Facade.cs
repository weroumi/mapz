﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;

namespace Assets.Character
{
    public class MonsterFacade 
    {
        public BaseCharacter monsterStatus;
        public string Reward;
        GameObject monster;
        Vector3 position;

        public void SpawnStart(GameObject monster, Vector3 pos)
        {
            this.monster = monster;
            this.position = pos;
            var dropdown = GameObject.Find("Level").GetComponent<TMP_Dropdown>();
            dropdown.onValueChanged.AddListener(delegate { DropdownItemSelected(dropdown); });
        }
        public MonsterFacade(GameObject monster)
        {
            this.monster = monster;
        }
        BaseCharacter CreateMonster(ICreateMonster Factory)
        {
            SpawnMonster.Instantiate(monster, position, Quaternion.identity);
            System.Random rnd = new System.Random();
            int num = rnd.Next();
            if (num % 2 == 0)
            {
                Reward = "Egg";
                return Factory.CreateMonsterEgg();
            }
            else
            {
                Reward = "Eye";
                return Factory.CreateMonsterEye();
            }
        }
        void DropdownItemSelected(TMP_Dropdown dropdown)
        {
            if (dropdown.options[dropdown.value].text.Equals("Easy"))
            {
                monsterStatus = CreateMonster(new CreateMonsterEasy());
            }
            else if (dropdown.options[dropdown.value].text.Equals("Hard"))
            {
                monsterStatus = CreateMonster(new CreateMonsterHard());
            }
            else return;
            GameObject.Find("txtMonsterName").GetComponent<TextMeshProUGUI>().text = monsterStatus.Name;
        }
    }
}
