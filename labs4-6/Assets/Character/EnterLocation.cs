using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnterLocation : MonoBehaviour
{
    private bool enterAllowed;
    private string sceneToLoad;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<University>())
        {
            sceneToLoad = "University";
            enterAllowed = true;
        }
        else if (collision.GetComponent<Visit>())
        {
            sceneToLoad = "Visit";
            enterAllowed = true;
        }
        else if (collision.GetComponent<Forest>())
        {
            sceneToLoad = "Forest";
            enterAllowed = true;
        }
        else if (collision.GetComponent<Portal>())
        {
            sceneToLoad = "SampleScene";
            enterAllowed = true;
        }
        else if (collision.GetComponent<Cemetery>()) 
        {
            sceneToLoad = "Mix";
            enterAllowed = true;
        }
        else
        {
            enterAllowed = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(enterAllowed && Input.GetKey(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}
