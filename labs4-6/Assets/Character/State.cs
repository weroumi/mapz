﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Potions;

namespace Assets.Character
{
    class PlayerContext
    {
        
        
    }
    public abstract class State
    {
        public abstract void CollectApple();
        public abstract void CollectRuby();
        public abstract void CollectMushroom();
        public abstract void CollectCheese();

    }
    public class Walk : State
    {
        public override void CollectApple()
        {
            Inventory.GetInstance().AppleCount = 2;
        }
        public override void CollectCheese()
        {
            Inventory.GetInstance().CheeseCount = 2;
        }
        public override void CollectMushroom()
        {
            Inventory.GetInstance().MushroomCount = 2;
        }
        public override void CollectRuby()
        {
            Inventory.GetInstance().RubyCount = 2;
        }
    }
    public class Run : State
    {
        public override void CollectApple()
        {
            Inventory.GetInstance().AppleCount = 1;
        }
        public override void CollectCheese()
        {
            Inventory.GetInstance().CheeseCount = 1;
        }
        public override void CollectMushroom()
        {
            Inventory.GetInstance().MushroomCount = 1;
        }
        public override void CollectRuby()
        {
            Inventory.GetInstance().RubyCount = 1;
        }
    }
}