using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Assets.Potions;
public class SpawnPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject man;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(man, transform.position, Quaternion.identity);
        StatusManager.instance.SpawnPlayerStart();
    }
    private void Update()
    {
        if(StatusManager.instance.health <= 0)
        {
            SceneManager.LoadScene("Death");
        }
        
    }
   
}
